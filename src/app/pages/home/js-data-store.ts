import { cloneDeep, get as _get, set as _set } from 'lodash';

let store = {};
let supressGetter = false;
let supressSetter = false;
const actions: { [key: string]: ActionListener[] } = {};

type ListenerTypes = "get" | "set";
type ListenerCallback = (path: string) => void;
interface ActionListener {
    path?: string;
    action: ListenerCallback;
}

interface PropertyMapItem {
    instance: any;
    propertyName: string;
    parent?: PropertyMapItem;
}

type DeepPartial<T> = {
    [P in keyof T]?: T[P] extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T[P] extends ReadonlyArray<infer U>
    ? ReadonlyArray<DeepPartial<U>>
    : DeepPartial<T[P]>
}

const callListener = (path: string, type: ListenerTypes) => {
    const getterActionListeners = actions[type];

    if (getterActionListeners != null) {
        for (let listener of getterActionListeners) {
            if (!listener.path || listener.path === path) {
                listener.action(path);
            }
        }
    }
}

const createProxy = (instance: any, path: string = "") => {
    return new Proxy(instance, {
        get: (target: any, p: string | symbol, receiver: any) => {

            if (supressGetter === false) {
                const fullPath = `${!!path ? `${path}.` : ""}${String(p)}`
                console.log('get', fullPath);
                callListener(fullPath, "get");
            }

            return target[p];
        },
        set: (target: any, p: string | symbol, value: any, receiver: any) => {

            // set before we call out changes
            target[p] = value;

            if (supressSetter === false) {

                // when we call the callbacks, do a setTimeout to push to end of call stack
                // to allow for other processing first
                const fullPath =  `${!!path ? `${path}.` : ""}${String(p)}`;
                console.log('set', `${!!path ? `${path}.` : ""}${String(p)}`, value);
                callListener(fullPath, "set");
            }

            return true
        }
    });
}

const stringifySelector = <T extends {}>(selector: (value: T) => any) => {
    const parts = selector.toString()
        .match(/return.*\;/)![0]
        .replace("return ", "")
        .replace(";", "")
        .split(".");

    return parts.splice(1, parts.length - 1).join(".");
}

export const on = <T extends {}>(type: ListenerTypes, callback: ListenerCallback, selector?: (value: T) => any) => {
    supressGetter = true;
    if (selector) {
        const path = stringifySelector(selector);

        if (!actions[type]) {
            actions[type] = [];
        }

        actions[type].push({ path, action: callback });
    }
    supressGetter = false;
}

const mapObject = (instance: any, onHasChildProperties?: (property: PropertyMapItem, children: PropertyMapItem[]) => PropertyMapItem[]) => {
    let properties = createPropertyList(instance);
    const terminationProperties: PropertyMapItem[] = [];

    // No recursion here, use linear list instead
    for (let i = 0; i < properties.length; i++) {
        const property = properties[i];
        const propertyValue = property.instance[property.propertyName];
        const childList = createPropertyList(propertyValue, property);

        if (childList.length > 0) {

            if (onHasChildProperties) {
                const childrenOfChildren = onHasChildProperties(property, childList);
                properties = properties.concat(childrenOfChildren);
            }

        } else {
            terminationProperties.push(property);
        }
    }

    return { properties, terminationProperties };
}

export const create = <T extends {}>(initialState: T) => {
    const newStore = initialState ?? {};
    const proxyStore: { [key: string | symbol]: any } = createProxy(newStore);

    supressGetter = true;
    supressSetter = true;

    mapObject(newStore, (property, childList) => {
        const propertyValue = property.instance[property.propertyName];
        const path = getPath(property);
        const proxyPart = createProxy(propertyValue, path);
        _set(proxyStore, path, proxyPart);

        return childList;
    });

    supressGetter = false;
    supressSetter = false;

    store = proxyStore;
}

const getPath = (item: PropertyMapItem) => {
    const parts = [];

    let check: PropertyMapItem | null = item;

    while (check != null) {
        parts.unshift(check.propertyName);
        if (check.parent != null) {
            check = check.parent;
            continue;
        }
        check = null;
    }

    return parts.join(".");
}

const createPropertyList = (instance: any, parent?: PropertyMapItem) => {

    if (typeof instance !== 'object') {
        return [];
    }

    return Object.keys(instance).map(propertyName => ({ instance, propertyName, parent } as PropertyMapItem));
}

export const set = <T extends {}>(state: DeepPartial<T>) => {
    supressGetter = true;
    const properties = mapObject(state, (_property, children) => children);

    for (let property of properties.terminationProperties) {
        const path = getPath(property);
        const propertyValue = property.instance[property.propertyName];
        _set(store, path, propertyValue);
    }

    supressGetter = false;
}

export const get = <T extends {}>(selector: (value: T) => any) => {

    supressGetter = true;
    const path = stringifySelector(selector);
    supressGetter = false;

    return cloneDeep(_get(store, path));
}